package marsrover;

public class Navigation {
	
	
	private Axis x;
	private Axis y;
	private Direction direction;

	public Navigation(Axis x, Axis y, Direction direction) {
		super();
		this.x = x;
		this.y = y;
		this.direction = direction;

	}
	
	

	public Axis getX() {
		return x;
	}

	public void setX(Axis x) {
		this.x = x;
	}

	public Axis getY() {
		return y;
	}

	public void setY(Axis y) {
		this.y = y;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public void moveForward() {
		move( direction );
	}
	
	
	public void moveBackward() {
		Direction reverseDirection = direction.getReverseDirection();
		move(reverseDirection);
	}
	
	public void move( Direction direction ) {
		switch (direction) {
		case NORTH: {
			y.moveForward();
			break;
		}

		case EAST: {
			x.moveForward();
			break;
		}

		case SOUTH: {
			y.moveBackward();
			break;
		}

		case WEST: {
			x.moveBackward();
			break;
		}

		default: {
			throw getUnknownDirectionException();
		}
		}

	}



	private RuntimeException getUnknownDirectionException() {
		return new RuntimeException("Unknown direction: " + direction);
	}

	public void turnLeft() {
		setDirection(getNewDirectionLeft());
	}

	public void turnRight() {
		setDirection(getNewDirectionRight());
	}

	private Direction getNewDirectionRight() {
		int newIndex = direction.getIndex() + 1;
		int directionCount = Direction.values().length;
		if (newIndex > (directionCount - 1)) {
			newIndex = 0;
		}
		return Direction.values()[newIndex];
	}

	private Direction getNewDirectionLeft() {
		int index = direction.getIndex();

		int newIndex = 0;
		if (index == 0) {
			newIndex = 3;
		} else {
			newIndex = direction.getIndex() - 1;
		}

		return Direction.values()[newIndex];
	}

}
