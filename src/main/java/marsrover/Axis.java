package marsrover;

public class Axis {
	
	private int value;
	private int limit;
	
	
	public Axis(int value, int valueMax) {
		super();
		this.value = value;
		this.limit = valueMax;
		
		// System.out.println( "Value max: "+valueMax );
	}
	
	
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public int getValueMax() {
		return limit;
	}
	public void setValueMax(int valueMax) {
		this.limit = valueMax;
	}
	
	public void moveForward() {
		move(1);

	}
	

	public void moveBackward() {
		move(-1);
	}

	private void move(int unit) {
		value = value + unit;
		if (value > limit) {
			value = 0;
		} else if (value < 0) {
			value = limit;
		}

	}
	
	

}
