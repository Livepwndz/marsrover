package marsrover;

public enum Direction {
	NORTH(0, "N"),
	EAST(1, "E"),
	SOUTH(2, "S"),
	WEST(3, "W");

	private int index;
	private String letter;

	Direction(int index, String letter) {
		this.index = index;
		this.letter = letter;
	}
	
	

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getLetter() {
		return letter;
	}

	public void setLetter(String letter) {
		this.letter = letter;
	}


    public Direction getReverseDirection() {
        return values()[(this.getIndex() + 2) % 4];
    }

	public static Direction getRandomDirection() {
		int randomIndex = (int) (Math.random()*10); // 1 - 10
		int directionCount = values().length;
		
		if( randomIndex > ( directionCount - 1 ) ) {
			return getRandomDirection();
		}
		
		return values()[ randomIndex ];
		
		
	}
	
	
	
	
}
