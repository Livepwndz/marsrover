package marsrover;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RoverTests {
	
	private Direction STARTING_DIRECTION;
	
	private Rover rover;
	@Before
	public void setup() {
		Axis x = new Axis( 1,1 );
		Axis y = new Axis( 1,1 );
		
		STARTING_DIRECTION = Direction.NORTH;
		Navigation nav = new Navigation( x,y,STARTING_DIRECTION);
		rover = new Rover( nav );

	}
	
	
	@Test(expected = RuntimeException.class)
	public void onReceivingInvalidCommandshouldThrowRuntimeException() {
		rover.receiveCommands(new char[] { 'K' });
	}
	
	@Test
	public void facingNorthShouldReceiveMultipleCommands() {
		int x = rover.getNav().getX().getValue();
		int y = rover.getNav().getY().getValue();

		int newXValue = x + 0;
		int newYValue = y + 0;
		rover.receiveCommands(new char[] { 'f', 'b', 'l', 'r' });
		Assert.assertTrue(
				rover.getNav().getY().getValue() == newXValue && rover.getNav().getY().getValue() == newYValue);
		Assert.assertTrue(rover.getNav().getDirection().equals(Direction.NORTH));

	}
	
	
	@Test
	public void shouldMoveForward() {
		rover.getNav().setDirection( Direction.NORTH );
		rover.getNav().setY( new Axis( 1,2 )); 
		
		int newX = rover.getNav().getX().getValue();
		
		int newY = rover.getNav().getY().getValue()+1;
		
		rover.getNav().moveForward();
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( STARTING_DIRECTION.equals( rover.getNav().getDirection() ) );
	}
	
	@Test
	public void shouldMoveForwardWrap() {
		rover.getNav().setDirection( Direction.NORTH );
		rover.getNav().setY( new Axis( 1,1 )); 
		
		int newX = rover.getNav().getX().getValue();
		int newY = 0;
		
		rover.getNav().moveForward();
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( STARTING_DIRECTION.equals( rover.getNav().getDirection() ) );
	}

	
	@Test
	public void shouldMoveForwardStartingDirectionEast() {
		rover.getNav().setX( new Axis( 1,2 )); 
		int newX = rover.getNav().getX().getValue()+1;
		int newY = rover.getNav().getY().getValue();
		
		
		
		STARTING_DIRECTION = Direction.EAST;
		rover.getNav().setDirection( STARTING_DIRECTION );
		rover.getNav().moveForward();
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( STARTING_DIRECTION.equals( rover.getNav().getDirection() ) );
	}
	
	@Test
	public void shouldMoveForwardStartingDirectionEastWrap() {
		
		int newX = 0;
		int newY = rover.getNav().getY().getValue();
		
		STARTING_DIRECTION = Direction.EAST;
		rover.getNav().setDirection( STARTING_DIRECTION );
		rover.getNav().moveForward();
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( STARTING_DIRECTION.equals( rover.getNav().getDirection() ) );
	}
	
	@Test
	public void shouldMoveForwardStartingDirectionSouth() {
		
		int newX = rover.getNav().getX().getValue();
		int newY = rover.getNav().getY().getValue()-1;
		STARTING_DIRECTION = Direction.SOUTH;
		rover.getNav().setDirection( STARTING_DIRECTION );
		rover.getNav().moveForward();
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( STARTING_DIRECTION.equals( rover.getNav().getDirection() ) );
	}
	
	@Test
	public void shouldMoveForwardStartingDirectionSouthWrap() {
		rover.getNav().getY().setValue(0);
		int newX = rover.getNav().getX().getValue();
		int newY = rover.getNav().getY().getValueMax();
		
		
		STARTING_DIRECTION = Direction.SOUTH;
		rover.getNav().setDirection( STARTING_DIRECTION );
		rover.getNav().moveForward();
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( STARTING_DIRECTION.equals( rover.getNav().getDirection() ) );
	}
	
	@Test
	public void shouldMoveForwardStartingDirectionWest() {
		
		int newX = rover.getNav().getX().getValue()-1;
		int newY = rover.getNav().getY().getValue();
		STARTING_DIRECTION = Direction.WEST;
		rover.getNav().setDirection( STARTING_DIRECTION );
		rover.getNav().moveForward();
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( STARTING_DIRECTION.equals( rover.getNav().getDirection() ) );
	}
	
	
	@Test
	public void shouldMoveForwardStartingDirectionWestWrap() {
		
		rover.getNav().setX( new Axis( 0,1 ));
		int newX = rover.getNav().getX().getValueMax();
		int newY = rover.getNav().getY().getValue();
		STARTING_DIRECTION = Direction.WEST;
		rover.getNav().setDirection( STARTING_DIRECTION );
		rover.getNav().moveForward();
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( STARTING_DIRECTION.equals( rover.getNav().getDirection() ) );
	}
	
	


	@Test
	public void shouldMoveBackward() {
		
		rover.getNav().setDirection( Direction.NORTH );
		
		int newX = rover.getNav().getX().getValue();
		int newY = rover.getNav().getY().getValue()-1;
		rover.getNav().moveBackward(); 
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( STARTING_DIRECTION.equals( rover.getNav().getDirection() ) );

	}
	
	@Test
	public void shouldMoveBackwardWrap() {
		
		rover.getNav().setDirection( Direction.NORTH );
		rover.getNav().setY( new Axis( 0,1) );
		
		int newX = rover.getNav().getX().getValue();
		int newY = rover.getNav().getY().getValueMax();
		rover.getNav().moveBackward(); 
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( STARTING_DIRECTION.equals( rover.getNav().getDirection() ) );

	}
	
	
	@Test
	public void shouldMoveBackwardStartingDirectionEast() {
		
		int newX = rover.getNav().getX().getValue()-1;
		int newY = rover.getNav().getY().getValue();
		STARTING_DIRECTION = Direction.EAST;
		rover.getNav().setDirection( STARTING_DIRECTION );
		rover.getNav().moveBackward();
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( STARTING_DIRECTION.equals( rover.getNav().getDirection() ) );
	}
	
	@Test
	public void shouldMoveBackwardStartingDirectionEastWrap() {
		
		int newX = rover.getNav().getX().getValue()-1;
		int newY = rover.getNav().getY().getValue();
		STARTING_DIRECTION = Direction.EAST;
		rover.getNav().setDirection( STARTING_DIRECTION );
		rover.getNav().moveBackward();
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( STARTING_DIRECTION.equals( rover.getNav().getDirection() ) );
	}
	
	@Test
	public void shouldMoveBackwardStartingDirectionSouth() {
		
		rover.getNav().setY( new Axis (1,2));
		int newX = rover.getNav().getX().getValue();
		int newY = rover.getNav().getY().getValue()+1;
		STARTING_DIRECTION = Direction.SOUTH;
		rover.getNav().setDirection( STARTING_DIRECTION );
		rover.getNav().moveBackward();
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( STARTING_DIRECTION.equals( rover.getNav().getDirection() ) );
	}
	
	@Test
	public void shouldMoveBackwardStartingDirectionSouthWrap() {
		rover.getNav().setY( new Axis( 1, 1));
		
		int newX = rover.getNav().getX().getValue();
		int newY = 0;
		STARTING_DIRECTION = Direction.SOUTH;
		rover.getNav().setDirection( STARTING_DIRECTION );
		rover.getNav().moveBackward();
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( STARTING_DIRECTION.equals( rover.getNav().getDirection() ) );
	}
	
	@Test
	public void shouldMoveBackwardStartingDirectionWest() {
		
		rover.getNav().setX( new Axis( 1, 2));
		int newX = rover.getNav().getX().getValue()+1;
		int newY = rover.getNav().getY().getValue();
		STARTING_DIRECTION = Direction.WEST;
		rover.getNav().setDirection( STARTING_DIRECTION );
		rover.getNav().moveBackward();
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( STARTING_DIRECTION.equals( rover.getNav().getDirection() ) );
	}
	
	@Test
	public void shouldMoveBackwardStartingDirectionWestWrap() {
		
		
		int newX = 0;
		int newY = rover.getNav().getY().getValue();
		STARTING_DIRECTION = Direction.WEST;
		rover.getNav().setDirection( STARTING_DIRECTION );
		rover.getNav().moveBackward();
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( STARTING_DIRECTION.equals( rover.getNav().getDirection() ) );
	}

	@Test
	public void shouldTurnLeft() {
		
		rover.getNav().setDirection( Direction.NORTH );
		
		int newX = rover.getNav().getX().getValue();
		int newY = rover.getNav().getY().getValue();
		rover.getNav().turnLeft(); 
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( Direction.WEST.equals( rover.getNav().getDirection() ) );

	}
	
	@Test
	public void shouldTurnLeftStartingWithWest() {
		
		rover.getNav().setDirection( Direction.WEST );
		
		int newX = rover.getNav().getX().getValue();
		int newY = rover.getNav().getY().getValue();
		rover.getNav().turnLeft(); 
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( Direction.SOUTH.equals( rover.getNav().getDirection() ) );

	}
	
	@Test
	public void shouldTurnLeftStartingWithSouth() {
		
		rover.getNav().setDirection( Direction.SOUTH );
		
		int newX = rover.getNav().getX().getValue();
		int newY = rover.getNav().getY().getValue();
		rover.getNav().turnLeft(); 
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( Direction.EAST.equals( rover.getNav().getDirection() ) );

	}
	
	@Test
	public void shouldTurnLeftStartingWithEast() {
		
		rover.getNav().setDirection( Direction.EAST );
		
		int newX = rover.getNav().getX().getValue();
		int newY = rover.getNav().getY().getValue();
		rover.getNav().turnLeft(); 
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( Direction.NORTH.equals( rover.getNav().getDirection() ) );

	}

	@Test
	public void shouldTurnRight() {
		
		rover.getNav().setDirection( Direction.NORTH );
		
		int newX = rover.getNav().getX().getValue();
		int newY = rover.getNav().getY().getValue();
		rover.getNav().turnRight(); 
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( Direction.EAST.equals( rover.getNav().getDirection() ) );

	}
	
	@Test
	public void shouldTurnRightStartingWithEast() {
		
		rover.getNav().setDirection( Direction.EAST );
		
		int newX = rover.getNav().getX().getValue();
		int newY = rover.getNav().getY().getValue();
		rover.getNav().turnRight(); 
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( Direction.SOUTH.equals( rover.getNav().getDirection() ) );

	}
	
	@Test
	public void shouldTurnRightStartingWithSouth() {
		
		rover.getNav().setDirection( Direction.SOUTH);
		
		int newX = rover.getNav().getX().getValue();
		int newY = rover.getNav().getY().getValue();
		rover.getNav().turnRight(); 
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( Direction.WEST.equals( rover.getNav().getDirection() ) );

	}
	
	@Test
	public void shouldTurnRightStartingWithWest() {
		
		rover.getNav().setDirection( Direction.WEST );
		
		int newX = rover.getNav().getX().getValue();
		int newY = rover.getNav().getY().getValue();
		rover.getNav().turnRight(); 
		Assert.assertTrue( newX == rover.getNav().getX().getValue() );
		Assert.assertTrue( newY == rover.getNav().getY().getValue() );
		Assert.assertTrue( Direction.NORTH.equals( rover.getNav().getDirection() ) );

	}
	
	
	



}
